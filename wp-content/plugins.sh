# Install plugins

### INSTALL PLUGINS FOR THE PROJECT GLOBALLY ###
echo "Installing global plugins"
#wp plugin install autodescription --activate --force --allow-root

### INSTALL PLUGINS FOR LOCAL ###
if [[ $1 == 'local' ]]; then
echo "Installing local plugins"
wp plugin install polylang --activate --force --allow-root
wp plugin install lazy-blocks --activate --force --allow-root
wp plugin install advanced-custom-fields --activate --force --allow-root
wp plugin install custom-post-type-ui --activate --force --allow-root
wp plugin install disable-comments --activate --force --allow-root
wp plugin install redirection --activate --force --allow-root
wp plugin install taxonomy-terms-order --activate --force --allow-root
wp plugin install theme-translation-for-polylang --activate --force --allow-root
wp plugin install tinymce-advanced --activate --force --allow-root
wp plugin install wp-migrate-db --activate --force --allow-root
wp plugin install autodescription --activate --force --allow-root
wp plugin install reset-meta-box-positions --activate --force --allow-root
wp plugin install wp-mailhog-smtp --activate --force --allow-root
wp plugin install gdpr-cookie-compliance --activate --force --allow-root
fi

### INSTALL PLUGINS FOR STAGING ###
if [[ $1 == 'staging' ]]; then
echo "Installing staging plugins"
#wp plugin install wp-migrate-db --activate --force --allow-root
fi

### INSTALL PLUGINS FOR STAGING ###
if [[ $1 == 'production' ]]; then
echo "Installing production plugins"
#wp plugin install wp-migrate-db --activate --force --allow-root
fi
